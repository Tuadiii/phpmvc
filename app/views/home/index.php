<div class="container">
    <div class="jumbotron mt-4">
        <h1 class="display-4">Hello, Welcome to my site!</h1>
        <p class="lead">Hello, My name is <?= $data['name']; ?></p>
        <hr class="my-4">
        <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
        <a class="btn" style="background-color: var(--bs-dark-text-emphasis); color: var(--bs-body-bg);" href="#">Learn more</a>
    </div>
</div>