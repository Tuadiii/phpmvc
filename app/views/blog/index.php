<div class="container mt-4">

    <div class="row">
        <div class="col-lg-6">
            <?php Flasher::flash(); ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <button type="button" class="btn btn-primary" id="formModalTambah" data-bs-toggle="modal" data-bs-target="#formModal">
                Tambah Data Blog
            </button>
            <br><br>
            <h3>Blog</h3>
            <ul class="list-group">
                <?php foreach ($data['blog'] as $blog) : ?>
                    <li class="list-group-item">
                        <?= $blog['penulis']; ?>
                        <a href="<?= BASEURL; ?>/blog/hapus/<?= $blog['id']; ?>" class="badge text-bg-danger text-decoration-none float-end m-1" onclick="return confirm('Yakin Ingin Menghapus?');">delete</a>
                        <a href="<?= BASEURL; ?>/blog/update/<?= $blog['id']; ?>" class="badge text-bg-success text-decoration-none float-end m-1" id="formModalUbah" data-bs-toggle="modal" data-bs-target="#formModal" data-id="<?= $blog['id']; ?>">update</a>
                        <a href="<?= BASEURL; ?>/blog/detail/<?= $blog['id']; ?>" class="badge text-bg-primary text-decoration-none float-end m-1">detail</a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="formModal" tabindex="-1" aria-labelledby="ModalTitle" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h1 class="modal-title fs-5" id="ModalTitle">Tambah Data Blog</h1>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="<?= BASEURL; ?>/blog/tambah" method="post">
                    <div class="form-group">
                        <label for="penulis" class="form-label">Penulis</label>
                        <input type="text" class="form-control" id="penulis" name="penulis">
                    </div>

                    <div class="form-group">
                        <label for="title" class="form-label">Judul</label>
                        <input type="text" class="form-control" id="title" name="title">
                    </div>

                    <div class="form-group">
                        <label for="tulisan" class="form-label">Tulisan</label>
                        <input type="text" class="form-control" id="tulisan" name="tulisan">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Tambah Data</button>
                </form>
            </div>
        </div>
    </div>
</div>