<?php
class User_model
{
    private $name = 'I Putu Permana Adi Putra';
    private $db;
    private $table = 'user';

    public function GetUser()
    {
        return $this->name;
    }

    public function __construct()
    {
        $this->db = new Database;
    }

    public function login($data)
    {
        $username = $data['username'];
        $query = 'SELECT * FROM ' . $this->table . ' WHERE username = :username';
        $this->db->query($query);
        $this->db->bind('username', $username);
        $user = $this->db->single(); // Anda perlu pastikan single() ada dalam objek Database
        $count = $this->db->rowCount(); // Anda perlu pastikan rowCount() ada dalam objek Database

        if ($count < 1) {
            return -1;
        }
        // header('Location:' . BASEURL . '/home/index');


        $passwordPost = $data["password"];
        $passwordDb = $user["password"];
        if (password_verify($passwordPost, $passwordDb)) {
            $_SESSION['login'] = true;
            return 1;
        } else {
            return -1;
        }
    }

    public function register($data)
    {
        $query = 'INSERT INTO ' . $this->table . '(username, password) VALUES(:username, :password)';
        $hashPassword = password_hash($data['password'], PASSWORD_DEFAULT);
        $this->db->query($query);
        $this->db->bind('username', $data['username']);
        $this->db->bind('password', $hashPassword);
        $this->db->execute();
        return $this->db->rowCount();
    }
}
