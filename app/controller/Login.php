<?php
class Login extends Controller
{
    public function index()
    {
        $data['title'] = 'Login';

        $this->view('templates/header', $data);
        $this->view('login/index', $data);
        $this->view('templates/footer');
    }

    public function login()
    {
        if ($this->model('User_model')->login($_POST) > 0) {
            header('Location:' . BASEURL . '/home/index');
        } else {
            header('Location:' . BASEURL . '/login/index');
        }
    }
}
